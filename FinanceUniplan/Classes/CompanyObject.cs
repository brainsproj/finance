﻿
using System.Collections.Generic;


namespace UnitransTools.Classes
{
    public class CompanyObject
    {
        public  string CompanyName { get; set; }

        public   int id { get; set; }
    }

    public static class Companies
    {
        public static List<CompanyObject> GetCompanyList()
        {
            List<CompanyObject> companylist = new List<CompanyObject>();

            var company = new CompanyObject()
            {
                CompanyName = "Unitrans",

                id = 1
            };

            companylist.Add(company);
            return companylist;
        }

        
    }
}