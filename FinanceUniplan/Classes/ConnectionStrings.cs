﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace UnitransTools.Classes
{
    public static class ConnectionStrings
    {
        public static string  AdConnectionString {
            get { return ConfigurationManager.ConnectionStrings["ADConnectionstring"].ToString(); }
        }

        public static string ADDomain
        {
            get { return ConfigurationManager.AppSettings["ADDomain"]; }
        }

        public static string DBConnectionStringV2 {
            get {return ConfigurationManager.ConnectionStrings["UnitransOpsConnectionStringV2"].ToString(); }
        }
        public static string DBConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["UnitransOpsConnectionString"].ToString(); }
        }

        public static string AuditloggingConnectionstring {
            get
            {
                return ConfigurationManager.ConnectionStrings["AuditLoggingConnectionString"].ToString();

            }
        }
    }
}