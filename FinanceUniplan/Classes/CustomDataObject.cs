﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace UnitransTools.Classes
{
    public class CustomDataObject
    {
        private Hashtable @params;

        private string SPName;

        private int Count = 0;
    
        /// <summary>
        /// Stored Procedure Name OR SQL Query
        /// </summary>
        /// <param name="StoredProcedureName_OR_SQLQuery"></param>
        /// <remarks></remarks>
        public CustomDataObject(string StoredProcedureName_OR_SQLQuery)
        {
            @params = new Hashtable();

            SPName = StoredProcedureName_OR_SQLQuery;
        }

        public void AddParameter(object Key, object Value)
        {
            @params.Add(Key, Value);
        }

        public void ClearParameters()
        {
            @params.Clear();
        }

        public DataTable GetDataTable()
        {

            using (SqlConnection SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {
                SQLConnection.Open();

                using (SqlCommand SQLCMD = new SqlCommand(SPName, SQLConnection))
                {
                    //Auditlogging.AuditLogging("Exec " + SPName, FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);

                    SQLCMD.CommandType = CommandType.StoredProcedure;

                    if (@params.Count > 0)
                    {
                        foreach (DictionaryEntry param in @params)
                        {
                            SQLCMD.Parameters.AddWithValue(param.Key.ToString(), param.Value.ToString());

                            //Auditlogging.AuditLogging("Parameter " + param.Key.ToString + "=N'" + param.Value.ToString, FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);
                        }
                    }
                    SqlDataReader SQLReader = SQLCMD.ExecuteReader();

                    DataTable SQLTable = new DataTable();

                    SQLTable.Load(SQLReader);

                    return SQLTable;

                }

            }

        }

        public object GetSQLScalarValue()
        {
            using (SqlConnection SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {
                SQLConnection.Open();

                using (SqlCommand SQLCMD = new SqlCommand(SPName, SQLConnection))
                {
                    //Auditlogging.AuditLogging("Exec " + SPName, FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);

                    SQLCMD.CommandType = CommandType.StoredProcedure;

                    if (@params.Count > 0)
                    {
                        foreach (DictionaryEntry param in @params)
                        {

                            SQLCMD.Parameters.AddWithValue(param.Key.ToString(), param.Value.ToString());

                            //Auditlogging.AuditLogging("Parameter " + param.Key.ToString + "=N'" + param.Value.ToString, FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);
                        }
                    }

                    using (SqlDataReader SQLReader = SQLCMD.ExecuteReader())
                    {
                        DataTable SQLTable = new DataTable();

                        SQLTable.Load(SQLReader);

                        return SQLTable;
                
                    }

                }

            }

        }

        public object GetSQLScalarValueFromSQLQuery()
        {

            string strRet = "";

            using (SqlConnection SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {

                try
                {

                    SQLConnection.Open();

                    //Auditlogging.AuditLogging("SQL Query " + SPName, FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);

                    using (SqlCommand SQLCMD = new SqlCommand(SPName, SQLConnection))
                    {
                        SQLCMD.CommandType = CommandType.Text;

                        if (@params.Count > 0)
                        {
                            foreach (DictionaryEntry param in @params)
                            {
                                SQLCMD.Parameters.AddWithValue(param.Key.ToString(), param.Value.ToString());
                                // Auditlogging.AuditLogging("Parameter " + param.Key.ToString + "=N'" + param.Value.ToString + "'", FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);
                            }
                        }

                        strRet = SQLCMD.ExecuteScalar().ToString();

                    }

                }
                catch (Exception ex)
                {
                    throw ;
                }

            }

            return strRet;
        }
    
        public void ExecuteQuery()
        {

            using (SqlConnection SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {
                SQLConnection.Open();

                SqlCommand SQLCMD = new SqlCommand(SPName, SQLConnection);

                SQLCMD.CommandType = CommandType.StoredProcedure;

                foreach (DictionaryEntry param in @params)
                {
                    SQLCMD.Parameters.AddWithValue(param.Key.ToString(), param.Value.ToString());
                }

                SQLCMD.ExecuteNonQuery();

            }

        }

        /// <summary>
        /// Used To Update Database from a SQL Query
        /// </summary>
        /// <remarks></remarks>
        public void ExecuteSQLNonQuery()
        {

            using (SqlConnection SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {
                SQLConnection.Open();

                using (SqlCommand SQLCMD = new SqlCommand(SPName, SQLConnection))
                {

                    //Auditlogging.AuditLogging("SQL Query " + SPName, FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);

                    SQLCMD.CommandType = CommandType.Text;

                    if (@params.Count > 0)
                    {
                        foreach (DictionaryEntry param in @params)
                        {
                            SQLCMD.Parameters.AddWithValue(param.Key.ToString(), param.Value.ToString());
                            // Auditlogging.AuditLogging("Parameter " + param.Key.ToString + "=N'" + param.Value.ToString + "'", FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);
                        }
                    }

                    SQLCMD.ExecuteNonQuery();

                }

            }

        }

        public DataTable GetDataTableFromSQLQuery()
        {

            if (string.IsNullOrEmpty(SPName))
                return null;

            using (SqlConnection SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {

                SQLConnection.Open();

                using (SqlCommand SQLCMD = new SqlCommand(SPName, SQLConnection))
                {
                    SQLCMD.CommandTimeout = 400;

                    SQLCMD.CommandType = CommandType.Text;

                    if (@params.Count > 0)
                    {
                        foreach (DictionaryEntry param in @params)
                        {
                            SQLCMD.Parameters.AddWithValue(param.Key.ToString(), param.Value.ToString());

                            //Auditlogging.AuditLogging("Parameter " + param.Key.ToString + "=N'" + param.Value.ToString + "'", FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("IFOXDisplayName").Value);
                        }
                    }

                    using (SqlDataReader SQLReader = SQLCMD.ExecuteReader())
                    {
                        DataTable SQLTable = new DataTable();

                        if (SQLReader.HasRows)
                        {
                            SQLTable.Load(SQLReader);

                            Count = SQLTable.Rows.Count;
                        }
                        else
                        {
                            Count = 0;
                        }
                        return SQLTable;
                    }

                }

            }


        }







    }
}

