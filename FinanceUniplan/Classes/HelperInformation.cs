﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using Newtonsoft.Json;

namespace UnitransTools.Classes
{
    public class HelpInformation
    {


        private bool blnDidAnErrorHappen = false;
        //set variables when the class is initialised
        private string strCurrentPage = string.Empty;

        private string strApplicationID = string.Empty;
        private DataTable dtableTooltips = null;

       // private bool blnTooltipGlobalVisible = false;
        private DataTable dtablePageInformation = null;

       // private bool blnPageInformationVisible = false;


        /// <summary>
        /// The beginning to insert tool tips on the page
        /// </summary>
        /// <remarks></remarks>

        public void Start()
        {
            strCurrentPage = GetCurrentPage();
            // strApplicationID = IFoxLib.GetApplicationIDPortNo().ToString();
            // Auditlogging.AuditLogging("strApplicationID  = " + strApplicationID, "Login/App_Code/HelpInformation.vb -- Start -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);

            if (Convert.ToInt32(strApplicationID) != 0)
            {
                //strApplicationID = "0" means that there was problem getting the correct port number from the url

                DataTable dtableTooltipGlobalSettings = GetGlobalSettingsFromDB(strApplicationID);
                DataTable dtableTooltipControlTypeSettings = GetToolTipControlTypeSettingsFromDB(strApplicationID);
                DataTable dtablePageInformationGlobalSettings = GetPageInformationGlobalSettingsFromDB(strApplicationID);


                try
                {

                    ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ///''''''''''''''''''''Tooltip''''''''''''''''''''''''''''''''''''''''''''''''
                    ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    //set tooltip global settings
                    //if global settings is true then set global variables

                    foreach (DataRow dr in dtableTooltipGlobalSettings.Rows)
                    {

                        if (Convert.ToBoolean(dr["Visible"]))
                        {
                            dtableTooltips = GetAllVisibleToolTipsFromDB(strApplicationID);
                            //get data from DB

                        }

                    }




                    ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ///''''''''''''''''''Page Information'''''''''''''''''''''''''''''''''''''''''
                    ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                    //set page information global settings
                    //add page help information only if it is visible in the global settings

                    foreach (DataRow drpgs in dtablePageInformationGlobalSettings.Rows)
                    {

                        if (Convert.ToBoolean(drpgs["Visible"]))
                        {
                            dtablePageInformation = GetPageInformationFromDB(strApplicationID);
                            // get data from DB
                        }
                        else
                        {
                            //dtablePageInformation = "[]"
                        }

                    }



                    ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    //only write the JSON objects to the page if there where no exceptions 

                    if (blnDidAnErrorHappen == false)
                    {
                        addHelpInfoJavascript(dtableTooltips, dtablePageInformation, dtableTooltipGlobalSettings, dtableTooltipControlTypeSettings);
                        // Auditlogging.AuditLogging("HelpInformation JSON added to page", FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies["DisplayName"].Value);
                    }
                    else
                    {
                        //Auditlogging.AuditLogging("ERROR: HelpInformation JSON was not added to the page  " + "blnDidAnErrorHappen=" + blnDidAnErrorHappen, FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                    }



                }
                catch (Exception ex)
                {
                    // Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- Start -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                }

            }
            else
            {
                // Auditlogging.AuditLogging("ERROR:  'strApplicationID = '0' means that there was problem getting the correct port number from the url ", "Login/App_Code/HelpInformation.vb -- Start -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
            }


        }



        /// <summary>
        /// Get all the visible tool tips from the data base for the curent page in this application that is loaded
        /// </summary>
        /// <param name="strAppId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private DataTable GetAllVisibleToolTipsFromDB(string strAppId)
        {

            DataTable dtable = new DataTable();


            try
            {
                string strSQL = string.Empty;

                strSQL += "SELECT H.ControlName,H.Tooltip ";
                strSQL += "FROM Help_Tooltip as H ";
                strSQL += "INNER JOIN [Application] as A ";
                strSQL += "ON H.ApplicationID = A.ApplicationID ";
                strSQL += "INNER JOIN dbo.pages as P ";
                strSQL += "ON P.PageID = H.PageID ";
                strSQL += "WHERE P.PageName = '" + strCurrentPage + "' AND A.ApplicationID = " + strAppId + " AND H.Visible = 1";


                //  CustomDataObject cdo = new CustomDataObject(strSQL);

                // dtable = cdo.GetDataTableFromSQLQuery;


            }
            catch (Exception ex)
            {
                // Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- GetAllVisibleToolTipsFromDB -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                blnDidAnErrorHappen = true;
            }

            return dtable;

        }

        private DataTable GetGlobalSettingsFromDB(string strAppId)
        {

            DataTable dt = new DataTable();


            try
            {
                string strSQL = "SELECT TOP 1 [Visible] FROM Help_TooltipGlobalSettings WHERE ApplicationID = " + strAppId;

                CustomDataObject cdo = new CustomDataObject(strSQL);
                dt = cdo.GetDataTableFromSQLQuery();

            }
            catch (Exception ex)
            {
                //Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- GetGlobalSettingsFromDB -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                blnDidAnErrorHappen = true;
            }

            return dt;

        }

        private DataTable GetToolTipControlTypeSettingsFromDB(string strAppId)
        {

            DataTable dt = new DataTable();

            try
            {
                string strSQL = string.Empty;

                strSQL += "SELECT RTRIM(C.[Control]) AS [HtmlTagName],RTRIM([Type]) AS [HtmlType],[Visible] ";
                strSQL += "FROM Help_TooltipControlTypeSettings AS T ";
                strSQL += "INNER JOIN Help_TooltipControlTypes  AS C ";
                strSQL += "ON T.ControlTypeID = C.ControlTypeID ";
                strSQL += "WHERE T.ApplicationID = " + strAppId + " AND  T.Visible = 1";

                CustomDataObject cdo = new CustomDataObject(strSQL);
                dt = cdo.GetDataTableFromSQLQuery();

            }
            catch (Exception ex)
            {
                //Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- GetToolTipControlTypeSettingsFromDB -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                blnDidAnErrorHappen = true;
            }

            return dt;

        }

        private DataTable GetPageInformationFromDB(string strAppId)
        {

            DataTable dt = new DataTable();


            try
            {
                string strSQL = string.Empty;
                strSQL += "SELECT TOP 1  I.PageInformation, I.Visible ";
                strSQL += "FROM [UnitransOps].[dbo].[Help_PageInformation] AS I ";
                strSQL += "inner join [UnitransOps].[dbo].[Pages] AS P ";
                strSQL += "ON I.PageID = P.PageID ";
                strSQL += "inner join [UnitransOps].[dbo].[Application] AS A ";
                strSQL += "ON I.ApplicationID = A.ApplicationID ";
                strSQL += "WHERE P.PageName = '" + strCurrentPage + "' and A.ApplicationID = " + strAppId + " and I.Visible = 1";

                CustomDataObject cdo = new CustomDataObject(strSQL);
                dt = cdo.GetDataTableFromSQLQuery();

            }
            catch (Exception ex)
            {
                //Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- GetPageInformationFromDB -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                blnDidAnErrorHappen = true;
            }

            return dt;

        }

        private DataTable GetPageInformationGlobalSettingsFromDB(string strAppId)
        {

            DataTable dt = new DataTable();


            try
            {
                string strSQL = "SELECT TOP 1 Visible FROM [UnitransOps].[dbo].[Help_PageInformationGlobalSettings] WHERE ApplicationID = " + strAppId;

                CustomDataObject cdo = new CustomDataObject(strSQL);
                dt = cdo.GetDataTableFromSQLQuery();

            }
            catch (Exception ex)
            {
                //Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- GetPageInformationGlobalSettingsFromDB -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                blnDidAnErrorHappen = true;
            }

            return dt;

        }

        /// <summary>
        /// Get the page file name from the url
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private string GetCurrentPage()
        {

            string strPage = string.Empty;


            try
            {
                strPage = HttpContext.Current.Request.Url.AbsolutePath.ToLower();

            }
            catch (Exception ex)
            {
                //Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- GetCurrentPage -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                blnDidAnErrorHappen = true;
            }

            return strPage;

        }


        /// <summary>
        /// add the pageinfo  and tooltips information to the current page as a javascript objects
        /// </summary>
        /// <param name="dtTooltip"></param>
        /// <param name="dtPageInfo"></param>
        /// <remarks></remarks>

        private void addHelpInfoJavascript(DataTable dtTooltip, DataTable dtPageInfo, DataTable dtTooltipGlobalSettings, DataTable dtTooltipControlTypeSettings2)
        {

            try
            {
                //convert datatables to json
                string jsonTooltip = JsonConvert.SerializeObject(dtTooltip, Formatting.Indented);
                string jsonPageInfo = JsonConvert.SerializeObject(dtablePageInformation, Formatting.Indented);
                string jsonTooltipGlobalSettings = JsonConvert.SerializeObject(dtTooltipGlobalSettings, Formatting.Indented);
                string jsonTooltipControlTypeSettings = JsonConvert.SerializeObject(dtTooltipControlTypeSettings2, Formatting.Indented);
                string strNewLine2 = Environment.NewLine + Environment.NewLine;


                string strScript = "";
                strScript += "<div id='questionMarkFilterBtn' style='display:none; left:950px; top:3px; width:65px; height:65px; position:absolute; z-index:auto; cursor:pointer; background:url(/Images/questionMark.jpg) no-repeat;'></div>";
                //this must be added for the scrape to work
                strScript += strNewLine2;
                strScript += "<script type='text/javascript'>";
                strScript += strNewLine2;

                //null means that the in Help_TooltipGlobalSettings visible is set false
                if (jsonTooltip == "null")
                {
                    strScript += "var jsonDtTooltips = [];";
                }
                else
                {
                    strScript += "var jsonDtTooltips = " + jsonTooltip + ";";
                }
                strScript += strNewLine2;

                //null means that the in Help_PageInformationGlobalSettings visible is set set false
                if (jsonPageInfo == "null")
                {
                    strScript += "var jsonDtPageInfo = [];";
                }
                else
                {
                    strScript += "var jsonDtPageInfo = " + jsonPageInfo + ";";
                }

                strScript += strNewLine2;
                strScript += "var jsonDtTooltipGlobalSettings = " + jsonTooltipGlobalSettings + ";";
                strScript += strNewLine2;
                strScript += "var jsonDtTooltipControlTypeSettings = " + jsonTooltipControlTypeSettings + ";";
                strScript += strNewLine2;
                strScript += "</script>";


                //add javascript to page
                Page currentPage = (Page)HttpContext.Current.Handler;
                currentPage.ClientScript.RegisterStartupScript(currentPage.GetType(), "HelpInformation", strScript);

            }
            catch (Exception ex)
            {
                // Auditlogging.AuditLogging("ERROR: " + ex.Message, "Login/App_Code/HelpInformation.vb -- addHelpInfoJavascript -- " + FileIO.FileSystem.GetName(HttpContext.Current.Request.Url.LocalPath), HttpContext.Current.Request.Cookies("DisplayName").Value);
                blnDidAnErrorHappen = true;
            }

        }

    }
}
