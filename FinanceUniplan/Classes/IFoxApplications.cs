﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;

namespace UnitransTools.Classes
{
    public class IFoxApplicationsObject
    {

        private string URLHost
        {
            get { return HttpContext.Current.Request.Url.Host; }
        }

        public string AppName { get; set; }

        public string AppPort { get; set; }

        public string CurrentHost
        {
            get { return URLHost; }
        }

        public string AltPort { get; set; }

        public int PeriodID { get; set; }

        public string AppUrl
        {
            get { return string.Format("https://{0}:{1}", URLHost, AppPort); }

        }

    }

    public class IFoxApplication
    {
        private int _userid { get; set; }
        // COMMENTED OUT FOR FY DROP DOWN DEPLOY
        //public bool _subcontractor { get; set; } // identifies user as subcontractor

        private string Query
        {
            get
            {
                return @"select a.Name,
       a.Port,
       a.ApplicationID,
       1 as PeriodID
from Application a
       join
     User_ApplicationRights r on r.ApplicationID = a.ApplicationID
where r.UserID = @UserID  
union
select a.Name,
       a.Port - 10 as Port,
       a.ApplicationID,
       0 as PeriodID
from Application a
       join
     User_ApplicationRights r on r.ApplicationID = a.ApplicationID
where r.UserID = @UserID  and a.Port between 62 and 65
order by PeriodID,a.ApplicationID";
            }
        }

        public IFoxApplication(int userId)
        {
            this._userid = userId;
            // COMMENTED OUT FOR FY DROP DOWN DEPLOY
            //_subcontractor = IsSubcontractor();  // see if user is subcontractor
        }

        public List<IFoxApplicationsObject> GetIfoxApplications()
        {
            var cookieManager = new CookieManager();
            var applist = new List<IFoxApplicationsObject>();

            using (var SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionStringV2))
            {
                try
                {

                    SQLConnection.Open();

                    var RetUserCMD = new SqlCommand("SP_User_ApplicationRights_Get", SQLConnection);
                    
                    //var RetUserCMD = new SqlCommand(Query, SQLConnection);

                    RetUserCMD.CommandType = CommandType.StoredProcedure;

                    RetUserCMD.Parameters.AddWithValue("@UserID", _userid);

                    SqlDataReader RetUserTable = RetUserCMD.ExecuteReader();

                    using (DataTable SQLTable = new DataTable())
                    {
                        SQLTable.Load(RetUserTable);

                        foreach (DataRow dr in SQLTable.Rows)
                        {
                            var apps = new IFoxApplicationsObject();

                            apps.AppName = dr["Name"].ToString();

                            apps.AppPort = dr["Port"].ToString();

                            apps.PeriodID = 0;
                            
                            applist.Add(apps);

                        }
                       
                    }


                }
                catch (Exception ex)
                {

                }

            }
            using (var SQLConnection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {
                try
                {

                    SQLConnection.Open();

                    var RetUserCMD = new SqlCommand("SP_User_ApplicationRights_Get", SQLConnection);

                    //var RetUserCMD = new SqlCommand(Query, SQLConnection);

                    RetUserCMD.CommandType = CommandType.StoredProcedure;

                    RetUserCMD.Parameters.AddWithValue("@UserID", _userid);

                    SqlDataReader RetUserTable = RetUserCMD.ExecuteReader();

                    using (DataTable SQLTable = new DataTable())
                    {
                        SQLTable.Load(RetUserTable);

                        foreach (DataRow dr in SQLTable.Rows)
                        {
                            var apps = new IFoxApplicationsObject();

                            apps.AppName = dr["Name"].ToString();

                            apps.AppPort = dr["Port"].ToString();

                            apps.PeriodID = 1;

                            applist.Add(apps);

                        }

                       
                    }


                }
                catch (Exception ex)
                {

                }

            }
            return applist;
        }
    }
}




