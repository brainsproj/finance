﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class ApproveFixedRevenueController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }
        [Route("api/approvefixedrevenue/getapprovelist")]
        [HttpPost]
        //public string CommitContracts([FromBody] string[] val)
        public HttpResponseMessage GetApproveList(Batching filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_RatesToApprove_Get_PerCustomerContract_Summary", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ContractIDList", filter.ContractID);
                        command.Parameters.AddWithValue("@CustomerIDList", filter.CustomerID);
                        command.Parameters.AddWithValue("@FromDate", filter.StartDate);
                        command.Parameters.AddWithValue("@ToDate", filter.EndDate);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {

                                RateID = Convert.ToInt32(x["RateID"]),
                                Contract = x["Contract"].ToString(),
                                Customer = x["Customer"].ToString(),
                                Description = x["Description"].ToString(),
                                RateType = x["RateType"].ToString(),
                                RateValue = x["RateValue"].ToString(),
                                RateBasedOn = x["RateBasedOn"].ToString(),
                                MeasurementUnit = x["MeasurementUnit"].ToString(),
                                RateStartDate = x["RateStartDate"].ToString(),
                                RateEndDate = x["RateEndDate"].ToString(),
                                RulesCategory = x["RulesCategory"].ToString(),
                                RulesCategoryValue = x["RulesCategoryValue"].ToString(),
                                NoOfAssets = Convert.ToInt32(x["No of Assets"]),
                                Active = Convert.ToInt32(x["Active"]),
                                Year = x["Yr"].ToString(),
                                Month = x["mn"].ToString(),
                                YearMonth = x["MMYYYY"].ToString(),
                                FirstDay = x["FirstDay"].ToString(),
                                LastDay = x["LastDay"].ToString(),
                                ContractID = Convert.ToInt32(x["ContractID"]),
                                CustomerID = Convert.ToInt32(x["CustomerID"]),
                                AvgAssetRate = x["AvgAssetRate"].ToString(),

                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
        }

    }
}
