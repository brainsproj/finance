﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class ApproveVariableRevenueController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }
        [Route("api/approvevariablerevenue/gettriplist")]
        [HttpPost]
        //public string CommitContracts([FromBody] string[] val)
        public HttpResponseMessage GetTripList(Batching filter)
        {
            try
            {

                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_Approval_Zones", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", filter.ContractID);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@FromDate", filter.StartDate);
                        command.Parameters.AddWithValue("@ToDate", filter.EndDate);
                        command.Parameters.AddWithValue("@Active", 1);
                        command.Parameters.AddWithValue("@Status", 10);
                        command.Parameters.AddWithValue("@SummaryDetail", "Detail");
                        command.Parameters.AddWithValue("@RateStatus", "Unapproved");

                        //filter options
                        if (Int32.Parse(filter.drpDestination) > 0)
                        {
                            command.Parameters.AddWithValue("@EndCustomerID", filter.drpDestination);
                        }
                        if (Int32.Parse(filter.drpDriver) > 0)
                        {
                            command.Parameters.AddWithValue("@DriverID", filter.drpDriver);
                        }
                        if (Int32.Parse(filter.drpMeasurementUnit) > 0)
                        {
                            command.Parameters.AddWithValue("@MeasurementUnitID", filter.drpMeasurementUnit);
                        }
                        if (Int32.Parse(filter.drpProduct) > 0)
                        {
                            command.Parameters.AddWithValue("@ProductID", filter.drpProduct);
                        }
                        if (Int32.Parse(filter.drpSubContractor) > 0)
                        {
                            command.Parameters.AddWithValue("@SubContractorID", filter.drpSubContractor);
                        }
                        if (Int32.Parse(filter.drpTrailer) > 0)
                        {
                            command.Parameters.AddWithValue("@TrailerID", filter.drpTrailer);
                        }
                        if (Int32.Parse(filter.drpTruck) > 0)
                        {
                            command.Parameters.AddWithValue("@TruckID", filter.drpTruck);
                        }
                        if (Int32.Parse(filter.drpUpliftPoint) > 0)
                        {
                            command.Parameters.AddWithValue("@UpliftPointID", filter.drpUpliftPoint);
                        }
                        if (!string.IsNullOrWhiteSpace(filter.txtSearch))
                        {
                            command.Parameters.AddWithValue("@SearchRefNo", filter.txtSearch);
                        }


                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                LineItem = Convert.ToInt32(x["LineItem"]),
                                RevenueID = Convert.ToInt32(x["RevenueID"]),
                                Row = Convert.ToInt32(x["Row"]),
                                TripNo = x["TripNo"].ToString(),
                                CustTripRefNo = x["CustTripRefNo"].ToString(),
                                Customer = x["Customer"].ToString(),
                                EndCustomer = x["End Customer"].ToString(),
                                UpliftPoint = x["Uplift Point"].ToString(),
                                Fleet = x["Fleet"].ToString(),
                                Product = x["Product"].ToString(),
                                MeasurementUnit = x["MeasurementUnit"].ToString(),
                                RevenueVolume = x["RevVolume"].ToString(),
                                Revenue = x["Revenue"].ToString(),
                                Rate = x["Rate"].ToString(),
                                RateBasedOn = x["RateBasedOn"].ToString(),
                                TripID = Convert.ToInt32(x["TripID"]),
                                DropID = Convert.ToInt32(x["DropID"]),
                                MeasurementUnitID = Convert.ToInt32(x["MeasurementUnitID"]),
                                RateBasedID = Convert.ToInt32(x["RateBasedID"]),
                                ZoneRateID = Convert.ToInt32(x["ZoneRateID"]),
                                OpsContractID = Convert.ToInt32(x["OpsContractID"]),
                                Description = x["Description"].ToString(),
                                MeasurementUnitDescriptionID = Convert.ToInt32(x["MeasurementUnitDescripID"]),
                                CustRefNo = x["CustRefNo"].ToString(),
                                CustDelRefNo = x["CustDelRefNo"].ToString(),
                                DropWithRev = Convert.ToInt32(x["DropWithRev"]),
                                ZRPID = Convert.ToInt32(x["ZRPID"]),
                                MeasurementID = Convert.ToInt32(x["MeasurementID"])
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
        }


    }
}
