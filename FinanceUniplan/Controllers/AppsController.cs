﻿using FinanceUniplan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FinanceUniplan.Controllers
{
    public class AppsController : ApiController
    {
        [Route("api/apps/getappid")]
        [HttpGet]
        public HttpResponseMessage GetAppID()
        {
            try
            {
                Unitrans.Common.Helpers.CookieManager.GetIfoxCookie cm = new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie();
                var appid = cm.ApplicationID;
                var res = new { Status = "Success", Data = appid };
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }

        }

        [Route("api/apps/updateappid")]
        [HttpPost]
        public HttpResponseMessage UpdateAppID(Default df)
        {
            try
            {
                Unitrans.Common.Helpers.CookieManager.SetIfoxCookie cm = new Unitrans.Common.Helpers.CookieManager.SetIfoxCookie();
                cm.ApplicationID(df.id.ToString());
                var res = new { Status = "Success", Data = df };
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }

        }
    }
}
