﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class BatchReportController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }
        // GET: api/InvoiceCreditNoteStatus

        [Route("api/batchreport/getbatched")]
        [HttpPost]
        public HttpResponseMessage GetBatched(BatchReportFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_Retrieve_Batches_Report", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@FromDate", filter.FromDate);
                        command.Parameters.AddWithValue("@ToDate", filter.ToDate);
                        command.Parameters.AddWithValue("@BatchStatus", 30);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                Customer = x["Customer"].ToString(),
                                BatchNo = x["BatchNo"].ToString(),
                                FinPeriod = x["Period"].ToString(),
                                Amount = x["Batch Amount"].ToString(),
                                InvoiceRefNo = x["CustInvRefNo"].ToString(),
                                InvoiceNo = x["InvoiceNo"].ToString(),
                                InvoiceDate = x["Date Batched"].ToString(),
                                DateBatched = x["Date Batched"].ToString(),
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }



        [Route("api/batchreport/getinvoiced")]
        [HttpPost]
        public HttpResponseMessage GetInvoiced(BatchReportFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_Retrieve_Batches_Report", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@FromDate", filter.FromDate);
                        command.Parameters.AddWithValue("@ToDate", filter.ToDate);
                        command.Parameters.AddWithValue("@BatchStatus", 40);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                Customer = x["Customer"].ToString(),
                                BatchNo = x["BatchNo"].ToString(),
                                FinPeriod = x["Period"].ToString(),
                                Amount = x["Batch Amount"].ToString(),
                                InvoiceRefNo = x["CustInvRefNo"].ToString(),
                                InvoiceNo = x["InvoiceNo"].ToString(),
                                InvoiceDate = x["Date Batched"].ToString(),
                                DateBatched = x["Date Batched"].ToString(),
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/batchreport/getproforma")]
        [HttpPost]
        public HttpResponseMessage GetProforma(BatchReportFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_Retrieve_Batches_ProformaByDates", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@FromDate", filter.FromDate);
                        command.Parameters.AddWithValue("@ToDate", filter.ToDate);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                Customer = x["Customer"].ToString(),
                                BatchNo = x["BatchNo"].ToString(),
                                FinPeriod = "1",
                                Amount = x["AMTNOTLIN_0"].ToString(),
                                InvoiceRefNo = x["ITMDES_0"].ToString(),
                                InvoiceNo = "-",
                                InvoiceDate = x["BatchDatetime"].ToString(),
                                DateBatched = x["BatchDatetime"].ToString(),
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }



        [Route("api/batchreport/getpending")]
        [HttpPost]
        public HttpResponseMessage GetPending(BatchReportFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_Retrieve_Batches_PendingByDates", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@FromDate", filter.FromDate);
                        command.Parameters.AddWithValue("@ToDate", filter.ToDate);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                Customer = x["Customer"].ToString(),
                                BatchNo = x["BatchNo"].ToString(),
                                FinPeriod = "1",
                                Amount = x["AMTNOTLIN_0"].ToString(),
                                InvoiceRefNo = x["ITMDES_0"].ToString(),
                                InvoiceNo = "-",
                                InvoiceDate = x["BatchDatetime"].ToString(),
                                DateBatched = x["BatchDatetime"].ToString(),
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/batchreport/getbatchdetails")]
        [HttpPost]
        public HttpResponseMessage GetBatchDetails(BatchReportFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_Retrieve_BatchDetails_Report", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@BatchNo", filter.BatchNumber);
                        command.Parameters.AddWithValue("@Variable", 1);
                        command.Parameters.AddWithValue("@Fixed", 1);
                        command.Parameters.AddWithValue("@ORC", 1);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                TripNo = x["Trip No"].ToString(),
                                RateType = x["RateType"].ToString(),
                                CustTripRefNo = x["CustTripRefNo"].ToString(),
                                QuoteRef = x["QuoteReference"].ToString(),
                                DateOffloaded = x["Date Off Loaded"].ToString(),
                                PODNo = x["POD Number"].ToString(),
                                EndCustomer = x["End Customer"].ToString(),
                                UpliftPoint = x["Uplift Point"].ToString(),
                                Driver = x["Driver"].ToString(),
                                Truck = x["Truck"].ToString(),
                                Trailer = x["Trailer"].ToString(),
                                Pup = x["Pup"].ToString(),
                                RevenueVolume = x["Revenue Volume"].ToString(),
                                MeasurementUnit = x["Measurement Unit"].ToString(),
                                Rate = x["Rate"].ToString(),
                                Revenue = x["Revenue"].ToString(),                    
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }



    }
}
