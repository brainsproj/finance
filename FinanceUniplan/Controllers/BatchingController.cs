﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class BatchingController : ApiController
    {

        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }

        [Route("api/batching/gettrailers")]
        [HttpGet]
        public HttpResponseMessage GetTrailers()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("select distinct TrailerInformation.TrailerID, TrailerInformation.FleetNo from TrailerInformation WHERE TrailerInformation.OpsContractID in (" + GetIfoxCookie.OperatingContractName + ")  order by dbo.TrailerInformation.FleetNo asc ", connection))
                    {
                        command.CommandType = CommandType.Text;

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                TrailerID = x["TrailerID"].ToString(),
                                TrailerNo = x["FleetNo"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/batching/gettrucks")]
        [HttpGet]
        public HttpResponseMessage GetTrucks()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("select distinct VehicleInformation.VehicleID,VehicleInformation.FleetNo from VehicleInformation WHERE VehicleInformation.OpsContractID in (" + GetIfoxCookie.OperatingContractName + ")  order by dbo.Vehicleinformation.FleetNo asc ", connection))
                    {
                        command.CommandType = CommandType.Text;

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                VehicleID = x["VehicleID"].ToString(),
                                Vehicle = x["FleetNo"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        [Route("api/batching/getproducts")]
        [HttpGet]
        public HttpResponseMessage GetProducts()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("select distinct productname.productid, productname.productname from productname inner join customerproducts on customerproducts.productid=productname.productid inner join LocationDescription on LocationDescription.LocationID=CustomerProducts.CustomerID where LocationDescription.opscontractid in (" + GetIfoxCookie.OperatingContractName + ")   order by dbo.Productname.ProductName asc", connection))
                    {
                        command.CommandType = CommandType.Text;

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                ProductID = x["productid"].ToString(),
                                ProductName = x["productname"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        [Route("api/batching/getdestination")]
        [HttpGet]
        public HttpResponseMessage GetDestination()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_GetLocations", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@LocationTypeID", 2);
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", 0);
                        using (SqlDataReader customers = command.ExecuteReader())
                        {
                            var customersList = customers.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                LocationID = Convert.ToInt32(x["LocationID"]),
                                CustomerName = x["Name"].ToString(),
                            });
                            return Request.CreateResponse(customersList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        [Route("api/batching/getdriver")]
        [HttpGet]
        public HttpResponseMessage GetDriver()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("select distinct EmployeeInformation.EmployeeID, EmployeeInformation.FirstName + ' ' + EmployeeInformation.LastName as 'Employee' from EmployeeInformation WHERE EmployeeInformation.OpsContractID  in (" + GetIfoxCookie.OperatingContractName + ")   Order by 'Employee' asc  ", connection))
                    {
                        command.CommandType = CommandType.Text;

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                EmployeeID = x["EmployeeID"].ToString(),
                                Employee = x["Employee"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/batching/upliftpoint")]
        [HttpGet]
        public HttpResponseMessage UpliftPoint()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_GetLocations", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@LocationTypeID", 4);
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", 0);
                        using (SqlDataReader customers = command.ExecuteReader())
                        {
                            var customersList = customers.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                LocationID = Convert.ToInt32(x["LocationID"]),
                                CustomerName = x["Name"].ToString(),
                            });
                            return Request.CreateResponse(customersList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/batching/unit")]
        [HttpGet]
        public HttpResponseMessage Unit()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("select dbo.RatesMeasurementUnit.MeasurementID, dbo.RatesMeasurementUnit.MeasurementUnit from dbo.RatesMeasurementUnit where MeasurementUnitDescripID in (1,2,3) order by dbo.RatesMeasurementUnit.MeasurementUnit asc", connection))
                    {
                        command.CommandType = CommandType.Text;

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                MeasurementUnitID = x["MeasurementID"].ToString(),
                                MeasurementUnit = x["MeasurementUnit"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        [Route("api/batching/subcontrator")]
        [HttpGet]
        public HttpResponseMessage SubContractor()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("select dbo.SubContractor.SubContractID, dbo.SubContractor.Name from dbo.SubContractor where dbo.SubContractor.OperatingContractID in (" + GetIfoxCookie.OperatingContractName + ")  order by dbo.SubContractor.Name asc", connection))
                    {
                        command.CommandType = CommandType.Text;

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                SubContractorID = x["SubContractID"].ToString(),
                                SubContractorName = x["Name"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/batching/gettriplist")]
        [HttpPost]
        //public string CommitContracts([FromBody] string[] val)
        public HttpResponseMessage GetTripList(Batching filter)
        {
            try
            {

                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_A_BatchRevenue_GetDetails_OnRoadCost", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", filter.ContractID);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@FromDate", filter.StartDate);
                        command.Parameters.AddWithValue("@ToDate", filter.EndDate);
                        command.Parameters.AddWithValue("@Variable", filter.Variable);
                        command.Parameters.AddWithValue("@Fixed", filter.Fixed);
                        command.Parameters.AddWithValue("@ORC", filter.ORC);

                        //filter options
                        if (Int32.Parse(filter.drpDestination) > 0)
                        {
                            command.Parameters.AddWithValue("@EndCustomerID", filter.drpDestination);
                        }
                        if (Int32.Parse(filter.drpDriver) > 0)
                        {
                            command.Parameters.AddWithValue("@DriverID", filter.drpDriver);
                        }
                        if (Int32.Parse(filter.drpMeasurementUnit) > 0)
                        {
                            command.Parameters.AddWithValue("@MeasurementUnitID", filter.drpMeasurementUnit);
                        }
                        if (Int32.Parse(filter.drpProduct) > 0)
                        {
                            command.Parameters.AddWithValue("@ProductID", filter.drpProduct);
                        }
                        if (Int32.Parse(filter.drpSubContractor) > 0)
                        {
                            command.Parameters.AddWithValue("@SubContractorID", filter.drpSubContractor);
                        }
                        if (Int32.Parse(filter.drpTrailer) > 0)
                        {
                            command.Parameters.AddWithValue("@TrailerID", filter.drpTrailer);
                        }
                        if (Int32.Parse(filter.drpTruck) > 0)
                        {
                            command.Parameters.AddWithValue("@TruckID", filter.drpTruck);
                        }
                        if (Int32.Parse(filter.drpUpliftPoint) > 0)
                        {
                            command.Parameters.AddWithValue("@UpliftPointID", filter.drpUpliftPoint);
                        }
                        if (!string.IsNullOrWhiteSpace(filter.txtSearch))
                        {
                            command.Parameters.AddWithValue("@SearchRefNo", filter.txtSearch);
                        }


                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                TripNo = x["Trip No"].ToString(),
                                RateType = x["RateType"].ToString(),
                                CustTripRefNo = x["CustTripRefNo"].ToString(),
                                QuoteRef = x["QuoteReference"].ToString(),
                                DateOffloaded = x["Date Off Loaded"].ToString(),
                                PODNo = x["POD Number"].ToString(),
                                EndCustomer = x["End Customer"].ToString(),
                                UpliftPoint = x["Uplift Point"].ToString(),
                                Driver = x["Driver"].ToString(),
                                Truck = x["Truck"].ToString(),
                                Trailer = x["Trailer"].ToString(),
                                Pup = x["Pup"].ToString(),
                                RevenueVolume = x["Revenue Volume"].ToString(),
                                MeasurementUnit = x["Measurement Unit"].ToString(),
                                Rate = x["Rate"].ToString(),
                                Revenue = x["Revenue"].ToString(),
                                RevenueID = Convert.ToInt32(x["RevenueID"]),
                                RateID = Convert.ToInt32(x["RateID"]),
                                DropID = Convert.ToInt32(x["DropID"]),
                                TripID = Convert.ToInt32(x["TripID"]),
                                DivisionID = Convert.ToInt32(x["DivisionID"]),
                                BatchNo = x["BatchNo"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
        }

        [Route("api/batching/batchtrips")]
        [HttpPost]
        public HttpResponseMessage BatchTrips(Batching b)
        {
            try
            {
                List<string> ls = new List<string>();
                List<BatchRevenue> batchrevenue = JsonConvert.DeserializeObject<List<BatchRevenue>>(b.BatchData);
                foreach (var item in batchrevenue)
                {
                    var output = JsonConvert.SerializeObject(item.RevenueIDs);
                    List<char> charsToRemove = new List<char>() { ' ', '"', '[', ']' };
                    foreach (char c in charsToRemove)
                    {
                        output = output.Replace(c.ToString(), String.Empty);
                    }
                    /* generate xml */
                    var s = "<Batching>";
                    string[] lines = output.Split(',');
                    var o = from line in
                                (
                                    from inner in lines
                                    select inner.Split(',')
                                )
                            select new
                            {
                                name = line[0]
                            };
                    foreach (var p in o)
                    {
                        if (p.name.ToString().Trim().Contains("F"))
                        {
                            s += "<L><FID>" + p.name.Replace("F", "") + "</FID></L>";
                        }
                        else if (p.name.ToString().Trim().Contains("O"))
                        {
                            s += "<L><OID>" + p.name.Replace("O", "") + "</OID></L>";
                        }
                        else
                        {
                            s += "<L><RID>" + p.name + "</RID></L>";
                        }
                    }
                    s += " </Batching>";
                    ///* end xml generation */

                    using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                    {
                        connection.Open();

                        using (var command = new SqlCommand("sp_A_BatchRevenue_OnRoadCost", connection))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@Batching", s);
                            command.Parameters.AddWithValue("@DivisionID", item.DivisionID);
                            command.Parameters.AddWithValue("@AlteredBy", GetIfoxCookie.IFOXUserID);
                            SqlParameter prmRetBatchNo = command.Parameters.Add("@RetBatchNo", SqlDbType.VarChar, 100);
                            int iRowsAffected = 0;
                            prmRetBatchNo.Direction = ParameterDirection.Output;
                            iRowsAffected = command.ExecuteNonQuery();
                            string strBatchNo = prmRetBatchNo.Value.ToString();
                            var pet = new { item.IndexIDs, strBatchNo};
                            ls.Add(pet.ToString());                 
                        }

                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, ls);
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex, BatchNumber = "" };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }



        //[Route("api/batching/batchtrips")]
        //[HttpPost]
        //public HttpResponseMessage BatchTrips(Batching b)
        //{
        //    try
        //    {
        //        var output = JsonConvert.SerializeObject(b.RevenueIDs);
        //        List<char> charsToRemove = new List<char>() { ' ', '"', '[', ']' };
        //        foreach (char c in charsToRemove)
        //        {
        //            output = output.Replace(c.ToString(), String.Empty);
        //        }
        //        /* generate xml */
        //        var s = "<Batching>";
        //        string[] lines = output.Split(',');
        //        var o = from line in
        //                    (
        //                        from inner in lines
        //                        select inner.Split(',')
        //                    )
        //                select new
        //                {
        //                    name = line[0]
        //                };
        //        foreach (var p in o)
        //        {
        //            if (p.name.ToString().Trim().Contains("F")) {
        //                s += "<L><FID>" + p.name.Replace("F", "") + "</FID></L>";
        //            } else if (p.name.ToString().Trim().Contains("O"))
        //            {
        //                s += "<L><OID>" + p.name.Replace("O", "") + "</OID></L>";
        //            } else
        //            {
        //                s += "<L><RID>" + p.name + "</RID></L>";
        //            }                  
        //        }
        //        s += " </Batching>";
        //        /* end xml generation */
        //        using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
        //        {
        //            connection.Open();

        //            using (var command = new SqlCommand("sp_A_BatchRevenue_OnRoadCost", connection))
        //            {
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.AddWithValue("@Batching", s);
        //                command.Parameters.AddWithValue("@DivisionID", b.DivisionID);
        //                command.Parameters.AddWithValue("@AlteredBy", GetIfoxCookie.IFOXUserID);
        //                SqlParameter prmRetBatchNo = command.Parameters.Add("@RetBatchNo", SqlDbType.VarChar, 100);
        //                int iRowsAffected = 0;
        //                prmRetBatchNo.Direction = ParameterDirection.Output;
        //                iRowsAffected = command.ExecuteNonQuery();            
        //                string strBatchNo = prmRetBatchNo.Value.ToString();
        //                var pet = new { Status = "Success", BatchNumber= strBatchNo, Message = "Batching completed successfully" };
        //                return Request.CreateResponse(HttpStatusCode.OK, pet);
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var pet = new { Status = "Error", Message = ex, BatchNumber="" };
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
        //    }
        //}*

        [Route("api/batching/addtobatch")]
        [HttpPost]
        public HttpResponseMessage AddToBatch(Batching b)
        {
            try
            {
                var output = JsonConvert.SerializeObject(b.RevenueIDs);
                List<char> charsToRemove = new List<char>() { ' ', '"', '[', ']' };
                foreach (char c in charsToRemove)
                {
                    output = output.Replace(c.ToString(), String.Empty);
                }
                /* generate xml */
                var s = "<Batching>";
                string[] lines = output.Split(',');
                var o = from line in
                            (
                                from inner in lines
                                select inner.Split(',')
                            )
                        select new
                        {
                            name = line[0]
                        };
                foreach (var p in o)
                {
                    if (p.name.ToString().Trim().Contains("F"))
                    {
                        s += "<L><FID>" + p.name.Replace("F", "") + "</FID></L>";
                    }
                    else if (p.name.ToString().Trim().Contains("O"))
                    {
                        s += "<L><OID>" + p.name.Replace("O", "") + "</OID></L>";
                    }
                    else
                    {
                        s += "<L><RID>" + p.name + "</RID></L>";
                    }
                }
                s += " </Batching>";
                /* end xml generation */
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_Atb_BatchRevenue_OnRoadCost", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Batching", s);
                        command.Parameters.AddWithValue("@DivisionID", b.DivisionID);
                        command.Parameters.AddWithValue("@AddToBatchNo", b.BatchNumber);
                        command.Parameters.AddWithValue("@AlteredBy", GetIfoxCookie.IFOXUserID);
                        SqlParameter prmRetBatchNo = command.Parameters.Add("@RetBatchNo", SqlDbType.VarChar, 100);
                        int iRowsAffected = 0;
                        prmRetBatchNo.Direction = ParameterDirection.Output;
                        iRowsAffected = command.ExecuteNonQuery();
                        string strBatchNo = prmRetBatchNo.Value.ToString();
                        var pet = new { Status = "Success", BatchNumber = strBatchNo, Message = "Batching completed successfully" };
                        return Request.CreateResponse(HttpStatusCode.OK, pet);
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex, BatchNumber = "" };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }


    }

}