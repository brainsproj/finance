﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class FixedRateController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }

        [Route("api/fixedrate/geteditrates")]
        [HttpPost]
        public HttpResponseMessage GetInvoiceStatuses(RatesFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_Rates_Get", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ContractID", filter.ContractID);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@Active", filter.Status);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                RateID = x["RateID"].ToString(),
                                Description = x["Description"].ToString(),
                                RateType = x["RateType"].ToString(),
                                RateValue = x["RateValue"].ToString(),
                                RateBasedOn = x["RateBasedOn"].ToString(),
                                MeasurementUnit = x["MeasurementUnit"].ToString(),
                                RateStartDate = x["RateStartDate"].ToString(),
                                RateEndDate = x["RateEndDate"].ToString(),
                                Category = x["RulesCategory"].ToString(),
                                CategoryDetails = x["RulesCategoryValue"].ToString(),
                                TotalAssets = x["No of Assets"].ToString(),
                                Active = x["Active"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/fixedrate/getvehiclespercontract")]
        [HttpPost]
        public HttpResponseMessage GetVehiclesPerContract(RatesFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_VehicleInformation_Get", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", filter.ContractID);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                VehicleID = Convert.ToInt32(x["VehicleID"]),
                                FleetNumber = x["FleetNo"].ToString(),
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
    }
}
