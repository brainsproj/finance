﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class InvoiceBatchedRevenueController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }

        [Route("api/invoicebatchedrevenue/getrevenue")]
        [HttpPost]
        public HttpResponseMessage GetRevenue(InvoiceBatchedRevenueFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_A_Retrieve_BatchesToInvoice_Sage", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", filter.CustomerID);
                        command.Parameters.AddWithValue("@SearchInput", filter.SearchText);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                BatchNumber = x["BatchNo"].ToString(),
                                BatchAmount = x["Batch Amount"].ToString(),
                                BatchedBy = x["Batched By"].ToString(),
                                DateBatched = x["Date Batched"].ToString(),
                                InvoiceNo = x["InvoiceNo"].ToString(),
                                FinancePeriod = x["FinancePeriod"].ToString(),
                                Customer = x["Customer"].ToString(),
                                DivisionID = x["DivisionID"].ToString(),
                                Period = x["Period"].ToString(),
                                SubRegionID = x["SubRegionID"].ToString(),
                                Company = x["Company"].ToString(),
                                GroupBatchNo = x["GroupBatchNo"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/invoicebatchedrevenue/updatebatches")]
        [HttpPost]
        public string UpdateBatches(InvoiceBatchedRevenue inv)

        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_A_InvoiceRevenue", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@BatchNo", inv.BatchNumber);
                        command.Parameters.AddWithValue("@InvoiceNo", "");
                        command.Parameters.AddWithValue("@CustInvRefNo", inv.CustInvRefNo);
                        command.Parameters.AddWithValue("@FinancePeriod", inv.FinancePeriod);
                        command.Parameters.AddWithValue("@AlteredBy", GetIfoxCookie.IFOXUserID);
                        command.ExecuteNonQuery();
                    }

                    using (var command = new SqlCommand("SAGE_sp_SAGESendLog", connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@BatchNo", inv.BatchNumber);
                        command.Parameters.AddWithValue("@InvoiceDate", inv.InvoiceDate);
                        command.Parameters.AddWithValue("@SAGEInvoiceTemplateID", inv.SAGEInvoiceTemplateID);
                        command.Parameters.AddWithValue("@CustomDescription", "1");
                        command.Parameters.AddWithValue("@ReadyForProcess", false);
                        command.Parameters.AddWithValue("@UserID", GetIfoxCookie.IFOXUserID);
                        command.ExecuteNonQuery();
                    }

                }

                return "success";
            }
            catch (Exception)
            {

                return "failed";
            }
        }



        [Route("api/invoicebatchedrevenue/groupbatches")]
        [HttpPost]
        public string GroupBatches(Batching b)

        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_GroupBatchNr", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@guid", b.GUID);
                        command.Parameters.AddWithValue("@batchNo", b.BatchNumber);
                        command.Parameters.AddWithValue("@createdBy", GetIfoxCookie.IFOXUserID);
                        command.ExecuteNonQuery();
                    }

                }

                return "success";
            }
            catch (Exception)
            {

                return "failed";
            }
        }



        [Route("api/invoicebatchedrevenue/ungroupbatches")]
        [HttpPost]
        public string UnGroupBatches(Batching b)

        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_UnGroupBatchNr", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@batchNo", b.BatchNumber);
                        command.ExecuteNonQuery();
                    }

                }

                return "success";
            }
            catch (Exception)
            {

                return "failed";
            }
        }

        [Route("api/invoicebatchedrevenue/gettriplist")]
        [HttpPost]
        //public string CommitContracts([FromBody] string[] val)
        public HttpResponseMessage GetTripList(BatchReportFilter filter)
        {
            try
            {
                var cm = new CookieManager.SetIfoxCookie();
                var output = JsonConvert.SerializeObject(filter.BatchNumber);
                List<char> charsToRemove = new List<char>() { ' ', '"', '[', ']'};
                foreach (char c in charsToRemove)
                {
                    output = output.Replace(c.ToString(), String.Empty);
                }

                var BNo = output.Replace("\\", "");

                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_Retrieve_BatchDetails_Print", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@BatchNo", BNo);
                        command.Parameters.AddWithValue("@Variable", 1);
                        command.Parameters.AddWithValue("@Fixed", 1);
                        command.Parameters.AddWithValue("@ORC", 1);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                TripNo = x["Trip No"].ToString(),
                                RateType = x["RateType"].ToString(),
                                CustTripRefNo = x["CustTripRefNo"].ToString(),
                                QuoteRef = x["QuoteReference"].ToString(),
                                DateOffloaded = x["Date Off Loaded"].ToString(),
                                PODNo = x["POD Number"].ToString(),
                                EndCustomer = x["End Customer"].ToString(),
                                UpliftPoint = x["Uplift Point"].ToString(),
                                Driver = x["Driver"].ToString(),
                                Truck = x["Truck"].ToString(),
                                Trailer = x["Trailer"].ToString(),
                                Pup = x["Pup"].ToString(),
                                RevenueVolume = x["Revenue Volume"].ToString(),
                                MeasurementUnit = x["Measurement Unit"].ToString(),
                                Rate = x["Rate"].ToString(),
                                Revenue = x["Revenue"].ToString(),
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }



    }
}
