﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class InvoiceController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }

        [Route("api/invoice/loadinvoices")]
        [HttpGet]
        public HttpResponseMessage LoadInvoices()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_InvoiceDetailCustomDescriptionSelect", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        using (SqlDataReader invoices = command.ExecuteReader())
                        {
                             var invoiceList = invoices.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                SAGEInvoiceDetailID = Convert.ToInt32(x["SAGEInvoiceDetailID"]),
                                SAGESendLogID = Convert.ToInt32(x["SAGESendLogID"]),
                                BatchNo = x["BatchNo"].ToString(),
                                CustInvRefNo = x["CustomerOrderNo"].ToString(),
                                ItemCode = x["Itmref_0"].ToString(),
                                dbItemDescription = x["Itmdes_0"].ToString(),
                                Rate = x["NetPri_0"].ToString(),
                                Qty = x["Qty_0"].ToString(),
                                InvoiceText_1 = x["CustomerLINETEXT_0_1"].ToString(),
                                InvoiceText_2 = x["CustomerLINETEXT_0_2"].ToString(),
                                InvoiceText_3 = x["CustomerLINETEXT_0_3"].ToString(),
                                InvoiceText_4 = x["CustomerLINETEXT_0_4"].ToString(),
                                InvoiceText_5 = x["CustomerLINETEXT_0_5"].ToString(),
                                InvoiceText_6 = x["CustomerLINETEXT_0_6"].ToString(),
                                InvoiceText_7 = x["CustomerLINETEXT_0_7"].ToString(),
                                UnitOfMeasure = x["SAU_0"].ToString(),
                                InvoiceDate = x["InvoiceDate"].ToString(),
                                Value = x["AMTNOTLIN_0"].ToString(),
                                ValueVat = x["AMTATILIN_0"].ToString(),
                                Site_Code = x["STOFCY_0"].ToString(),
                                Price_Vat = x["AMTTAXLIN_0"].ToString(),
                                Price_Exclude_Vat = x["AMTNOTLIN_0"].ToString(),
                                Price_Include_Vat = x["AMTATILIN_0"].ToString(),
                                Vat_Percentage = x["RATTAXLIN_0"].ToString(),
                                InvoiceID = Convert.ToInt32(x["SAGESendLogID"]),
                                Contract_Code = x["CCE_0"].ToString(),
                                Custom_Item_Description = x["CustomITMDES_0"].ToString(),
                                Customer_Code = x["CstCode"].ToString(),
                                Decimals = x["Decimals"].ToString(),
                                ContractID = x["ContractID"].ToString(),
                                InvContractCode = x["InvContractCode"].ToString(),
                                VatStatus = x["VatStatus"].ToString(),
                                CustomerID = x["CustomerID"].ToString(),
                                InvRefNumber = x["InvRefNumber"].ToString(),
                                InvDate = x["InvDate"].ToString(),
                                 isInvoiceChecked = false
                            });

                            return Request.CreateResponse(invoiceList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        [Route("api/invoice/getsingleinvoicedetails")]
        [HttpPost]
        public HttpResponseMessage GetSingleInvoiceDetails(InvoiceLineItem inv)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_InvoiceDetailProcessed", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@SageSendLogID", inv.id);
                        using (SqlDataReader invoices = command.ExecuteReader())
                        {
                            var invoiceList = invoices.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                SAGEInvoiceDetailID = Convert.ToInt32(x["SAGEInvoiceDetailID"]),
                                SAGESendLogID = Convert.ToInt32(x["SAGESendLogID"]),
                                BatchNo = x["BatchNo"].ToString(),
                                CustInvRefNo = x["CustomerOrderNo"].ToString(),
                                ItemCode = x["Itmref_0"].ToString(),
                                dbItemDescription = x["Itmdes_0"].ToString(),
                                Rate = x["NetPri_0"].ToString(),
                                Qty = x["Qty_0"].ToString(),
                                InvoiceText_1 = x["CustomerLINETEXT_0_1"].ToString(),
                                InvoiceText_2 = x["CustomerLINETEXT_0_2"].ToString(),
                                InvoiceText_3 = x["CustomerLINETEXT_0_3"].ToString(),
                                InvoiceText_4 = x["CustomerLINETEXT_0_4"].ToString(),
                                InvoiceText_5 = x["CustomerLINETEXT_0_5"].ToString(),
                                InvoiceText_6 = x["CustomerLINETEXT_0_6"].ToString(),
                                InvoiceText_7 = x["CustomerLINETEXT_0_7"].ToString(),
                                UnitOfMeasure = x["SAU_0"].ToString(),
                                InvoiceDate = x["InvoiceDate"].ToString(),
                                Value = x["AMTNOTLIN_0"].ToString(),
                                ValueVat = x["AMTATILIN_0"].ToString(),
                                Site_Code = x["STOFCY_0"].ToString(),
                                Price_Vat = x["AMTTAXLIN_0"].ToString(),
                                Price_Exclude_Vat = x["AMTNOTLIN_0"].ToString(),
                                Price_Include_Vat = x["AMTATILIN_0"].ToString(),
                                Vat_Percentage = x["RATTAXLIN_0"].ToString(),
                                InvoiceID = Convert.ToInt32(x["SAGESendLogID"]),
                                Contract_Code = x["CCE_0"].ToString(),
                                Custom_Item_Description = x["CustomITMDES_0"].ToString(),
                                Customer_Code = x["CstCode"].ToString(),
                                Decimals = x["Decimals"].ToString(),
                                ContractID = x["ContractID"].ToString(),
                                InvContractCode = x["InvContractCode"].ToString(),
                                VatStatus = x["VatStatus"].ToString(),
                                CustomerID = x["CustomerID"].ToString(),
                                InvRefNumber = x["InvRefNumber"].ToString(),
                                InvDate = x["InvDate"].ToString(),
                                isInvoiceChecked = false
                            });

                            return Request.CreateResponse(invoiceList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        [Route("api/invoice/loadcustomers")]
        [HttpGet]
        public HttpResponseMessage LoadCustomers()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_GetLocations", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@LocationTypeID", 1);
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", 0);
                        using (SqlDataReader invoices = command.ExecuteReader())
                        {
                            var customers = invoices.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                CustomerID = x["LocationID"].ToString(),
                                CustomerName = x["Name"].ToString()               
                            });

                            return Request.CreateResponse(customers);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        [Route("api/invoice/updatelineitems")]
        [HttpPost]
        public string UpdateLineItems(InvoiceLineItem inv)

        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_InvoiceDetailCustomDescriptionItemUpdate", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@SAGEInvoiceDetailID", inv.id);
                        command.Parameters.AddWithValue("@CustomItemText", inv.item_description);
                        command.Parameters.AddWithValue("@ContractCode", inv.contract_code);
                        command.Parameters.AddWithValue("@InvoiceText1", inv.extra1);
                        command.Parameters.AddWithValue("@InvoiceText2", inv.extra2);
                        command.Parameters.AddWithValue("@InvoiceText3", inv.extra3);
                        command.Parameters.AddWithValue("@InvoiceText4", inv.extra4);
                        command.Parameters.AddWithValue("@InvoiceText5", inv.extra5);
                        command.Parameters.AddWithValue("@InvoiceText6", inv.extra6);
                        command.Parameters.AddWithValue("@InvoiceText7", inv.extra7);
                        command.Parameters.AddWithValue("@ItemCode", inv.item_code);
                        command.Parameters.AddWithValue("@UserID", GetIfoxCookie.IFOXUserID);
                        command.ExecuteNonQuery();
                    }

                }

                return "success";
            }
            catch (Exception)
            {

                return "Failed";
            }
        }

        [Route("api/invoice/submitinvoices")]
        [HttpPost]
        public string SubmitInvoices([FromBody] string[] val)
        {

            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_InvoiceDetailCustomDescriptionUpdateSendLog", connection))
                    {
                        var output = JsonConvert.SerializeObject(val);
                        List<char> charsToRemove = new List<char>() { ' ', '"', '[', ']' };
                        foreach (char c in charsToRemove)
                        {
                            output = output.Replace(c.ToString(), String.Empty);
                        }
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@SageSendLogIds", output);
                        command.Parameters.AddWithValue("@UserID", GetIfoxCookie.IFOXUserID);
                        command.ExecuteNonQuery();
                    }

                }

                return "success";
            }
            catch (Exception)
            {
                return "failed";
            }
        }


        [Route("api/invoice/getlineitem")]
        [HttpPost]
        public HttpResponseMessage GetLineItem(InvoiceLineItem inv)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_InvoiceDetailGetLineItem", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@SAGEInvoiceDetailID", inv.id);
                        using (SqlDataReader invoices = command.ExecuteReader())
                        {
                            var invoiceList = invoices.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {

                                id = Convert.ToInt32(x["SAGEInvoiceDetailID"]),
                                item_code = String.IsNullOrEmpty(x["CustomerITMREF_0"].ToString()) ? x["Item_Code"].ToString() : x["CustomerITMREF_0"].ToString(),
                                extra1 = x["CustomerLINETEXT_0_1"].ToString(),
                                extra2 = x["CustomerLINETEXT_0_2"].ToString(),
                                extra3 = x["CustomerLINETEXT_0_3"].ToString(),
                                extra4 = x["CustomerLINETEXT_0_4"].ToString(),
                                extra5 = x["CustomerLINETEXT_0_5"].ToString(),
                                extra6 = x["CustomerLINETEXT_0_6"].ToString(),
                                extra7 = x["CustomerLINETEXT_0_7"].ToString(),
                                contract_code = x["contract_code"].ToString(),
                                item_description = String.IsNullOrEmpty(x["Custom_Item_Description"].ToString()) ? x["dbItemDescription"].ToString() : x["Custom_Item_Description"].ToString()

                            });

                            return Request.CreateResponse(invoiceList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }

        }


        // GET: api/Invoice
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Invoice/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Invoice
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Invoice/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Invoice/5
        public void Delete(int id)
        {
        }
    }
}
