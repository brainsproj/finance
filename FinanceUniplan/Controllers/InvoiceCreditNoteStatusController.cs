﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class InvoiceCreditNoteStatusController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }
        // GET: api/InvoiceCreditNoteStatus

        [Route("api/invoicecreditnotestatus/getinvoicestatuses")]
        [HttpPost]
        public HttpResponseMessage GetInvoiceStatuses(DatesFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SAGE_sp_Retrieve_SageInvoiceFeedBackByDates", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@FromDate", filter.StartDate);
                        command.Parameters.AddWithValue("@ToDate", filter.EndDate);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                MSGID = x["MSGID_0"].ToString(),
                                SentToSageDateTime = x["SentToSageDatetime"].ToString(),
                                ErrorMessage = x["ERRORMSG_0"].ToString(),
                                Status = x["MSGSTATUS_0"].ToString(),
                                ResetStatus = x["RESET_STATUS"].ToString(),
                                CustomerID = x["CustomerID"].ToString(),
                                InvoiceRef = x["CustomerOrderNo"].ToString(),
                                ProformaID = x["SAGESendLogID"].ToString(),
                                OperatingContractName = x["OperatingContractName"].ToString(),
                                BatchNo = x["BatchNo"].ToString(),
                                TotalCost = x["TotalAmount"].ToString(),
                                InvoiceDate = x["InvoiceDate"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }



        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/InvoiceCreditNoteStatus/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/InvoiceCreditNoteStatus
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/InvoiceCreditNoteStatus/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/InvoiceCreditNoteStatus/5
        public void Delete(int id)
        {
        }
    }
}
