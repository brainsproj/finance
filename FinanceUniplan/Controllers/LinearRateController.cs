﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class LinearRateController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }

        [Route("api/linearrate/getrates")]
        [HttpPost]
        public HttpResponseMessage GetInvoiceStatuses(RatesFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_S_RetrieveZoneRates_Linear", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractID", filter.ContractID);
                        command.Parameters.AddWithValue("@LocationID", filter.CustomerID);
                        command.Parameters.AddWithValue("@Active", filter.Status);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                ZoneRateID = x["ZoneRateID"].ToString(),
                                BeginDate = x["BeginDate"].ToString(),
                                EndDate = x["EndDate"].ToString(),
                                Customer = x["Customer"].ToString(),
                                From = x["From"].ToString(),
                                To = x["To"].ToString(),
                                QuoteRef = x["QuoteRef"].ToString(),
                                Description = x["Description"].ToString(),
                                MeasurementUnit = x["MeasurementUnit"].ToString(),
                                LeadValue = x["LeadValue"].ToString(),
                                LoadValue = x["LoadValue"].ToString(),
                                Adhoc = x["Adhoc"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }




        // GET: api/LinearRate
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LinearRate/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LinearRate
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/LinearRate/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LinearRate/5
        public void Delete(int id)
        {
        }
    }
}
