﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;
using System;

namespace FinanceUniplan.Controllers
{
    public class MainRateController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }

        [Route("api/mainrate/getrates")]
        [HttpPost]
        public HttpResponseMessage GetInvoiceStatuses(RatesFilter filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_S_RetrieveZoneRates", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractID", filter.ContractID);
                        command.Parameters.AddWithValue("@LocationID", filter.CustomerID);
                        command.Parameters.AddWithValue("@Active", filter.Status);

                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                ZoneRateID = x["ZoneRateID"].ToString(),
                                BeginDate = x["Begin Date"].ToString(),
                                EndDate = x["End Date"].ToString(),
                                Customer = x["Customer"].ToString(),
                                From = x["From"].ToString(),
                                To = x["To"].ToString(),
                                QuoteRef = x["Quote Ref"].ToString(),
                                Description = x["Description"].ToString(),
                                MeasurementUnit = x["Measurement Unit"].ToString(),
                                RateValue = x["RateValue"].ToString(),
                                Adhoc = x["Adhoc"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
    }
}
