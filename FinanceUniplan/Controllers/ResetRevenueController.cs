﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class ResetRevenueController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }

        // GET: api/ResetRevenue
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ResetRevenue/5
        public string Get(int id)
        {
            return "value";
        }
        [Route("api/resetrevenue/searchbatch")]
        [HttpPost]
        public string SearchBatch(ResetRevenue reset)

        {

            try
            {
                return "Success";
            }
            catch (Exception)
            {

                return "Failed to Add!!";
            }
        }

        [Route("api/resetrevenue/getresetitems")]
        [HttpPost]
        public HttpResponseMessage GetResetItems(RevenueResetFilters filter)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_A_Reset_DisplayAllFinancialStatuses", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@QueryType", filter.category);
                        command.Parameters.AddWithValue("@SearchText", filter.searchtext);
                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                Status = x["Status"].ToString(),
                                Contract = x["Contract"].ToString(),
                                Customer = x["Customer"].ToString(),
                                RateType = x["RateType"].ToString(),
                                BatchNo = x["BatchNo"].ToString(),
                                InvoiceNo = x["InvoiceNo"].ToString(),
                                Column1 = x["Column1"].ToString(),
                                TotalAmount = x["TotalAmount"].ToString(),
                                TotalRecords = x["TotalRecords"].ToString(),
                                MaxDate = x["MaxDate"].ToString(),
                                Allowreset = x["Allowreset"].ToString(),
                                AllowResetDescription = x["AllowResetDescription"].ToString(),
                                RevenueID = x["RevenueID"].ToString()
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
        [Route("api/resetrevenue/revenuereset")]
        [HttpPost]
        public string RevenueReset(ResetRevenue rst)

        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_Approvals_ResetRevenue_ALL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
    
                        if (String.IsNullOrEmpty(rst.BatchNumber)) {
                            command.Parameters.AddWithValue("@CurrentStatus", rst.CurrentStatus);
                            command.Parameters.AddWithValue("@NewStatus", rst.NewStatus);
                            command.Parameters.AddWithValue("@QueryType", rst.QueryType);
                            command.Parameters.AddWithValue("@Queryvalue", rst.QueryValue);
                            command.Parameters.AddWithValue("@RevenueID", rst.RevenueID);
                            command.Parameters.AddWithValue("@UserID", GetIfoxCookie.IFOXUserID);

                        } else {
                            command.Parameters.AddWithValue("@CurrentStatus", rst.CurrentStatus);
                            command.Parameters.AddWithValue("@NewStatus", rst.NewStatus);
                            command.Parameters.AddWithValue("@QueryType", rst.QueryType);
                            command.Parameters.AddWithValue("@Queryvalue", rst.QueryValue);
                            command.Parameters.AddWithValue("@BatchNo", rst.BatchNumber);
                            command.Parameters.AddWithValue("@RevenueID", rst.RevenueID);
                            command.Parameters.AddWithValue("@UserID", GetIfoxCookie.IFOXUserID);
                        }
                        //command.ExecuteNonQuery();
                    }

                }

                return "success";
            }
            catch (Exception)
            {

                return "Failed";
            }
        }
        // POST: api/ResetRevenue
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ResetRevenue/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ResetRevenue/5
        public void Delete(int id)
        {
        }
    }
}
