﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.BusinessLayer.Contracts;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;
using FinanceUniplan.Models;

namespace FinanceUniplan.Controllers
{
    public class SelectContractController : ApiController
    {
        public object ResData { get; private set; }
        public object AddMessage { get; private set; }
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }
        // GET api/selectcontract
        public HttpResponseMessage Get()
        {
            try
            {
                    using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                    {
                        connection.Open();

                        using (var command = new SqlCommand("SP_Retrieve_Contracts_ForUser", connection))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@UserID", GetIfoxCookie.IFOXUserID);
                            using (SqlDataReader contracts = command.ExecuteReader())
                            {

                                var contractList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new 
                                {
                                    id = Convert.ToInt32(x["OperatingContractID"]),
                                    ContractId = Convert.ToInt32(x["OperatingContractID"]),
                                    ContractName = x["Contract Name"].ToString(),
                                    Region = x["Region"].ToString(),
                                    Division = x["Division"].ToString(),
                                    DivisionId = Convert.ToInt32(x["DivisionID"]),
                                    OperatingOfficeName = x["Operating Office"].ToString(),
                                    OpsOfficeID = Convert.ToInt32(x["OpsOfficeID"]),
                                    RegionId = Convert.ToInt32(x["RegionID"]),
                                    SubRegion = x["Sub Region"].ToString(),
                                    SubRegionId = Convert.ToInt32(x["SubRegionID"]),
                                    ContractAbbr = x["ContractAbbr"].ToString()

                                });
                            //var addmsg = new { Status = "Success", Message = "" };
                            //var pet = new { Data = ResData = contractList, AdditionalParameters = AddMessage = addmsg };
                            return Request.CreateResponse(contractList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
        [Route("api/selectcontract/loadselectedcontracts")]
        [HttpGet]
        public HttpResponseMessage LoadSelectedContracts()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_Retrieve_OpsContractNames_By_OpsContractList", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var contractList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new 
                            {
                                id = Convert.ToInt32(x["OperatingContractID"]),
                                ContractId = Convert.ToInt32(x["OperatingContractID"]),
                                ContractName = x["OperatingContractName"].ToString(),
                                Region = x["Region"].ToString(),
                                Division = x["Division"].ToString(),
                                DivisionId = Convert.ToInt32(x["DivisionID"]),
                                OperatingOfficeName = x["OperatingOffice"].ToString(),
                                OpsOfficeID = Convert.ToInt32(x["OpsOfficeID"]),
                                RegionId = Convert.ToInt32(x["RegionID"]),
                                SubRegion = x["SubRegion"].ToString(),
                                SubRegionId = Convert.ToInt32(x["SubRegionID"]),
                                ContractAbbr = x["ContractAbbr"].ToString()
                            });

                            return Request.CreateResponse(contractList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
        [Route("api/selectcontract/loadcustomersbycontracts")]
        [HttpGet]
        public HttpResponseMessage LoadCustomersByContract()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("sp_GetLocations", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@LocationTypeID", 1);
                        command.Parameters.AddWithValue("@OpsContractIDList", GetIfoxCookie.OperatingContractName);
                        command.Parameters.AddWithValue("@CustomerID", 0);
                        using (SqlDataReader customers = command.ExecuteReader())
                        {
                            var customersList = customers.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                LocationID = Convert.ToInt32(x["LocationID"]),
                                CustomerName = x["Name"].ToString(),
                            });
                            return Request.CreateResponse(customersList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        [Route("api/selectcontract/loadcustomersforcontracts")]
        [HttpPost]
        public HttpResponseMessage LoadCustomersForContracts(Default d)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("SP_GetLocations_By_OpsContractID_LocationType", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OpsContractID", d.id);
                        command.Parameters.AddWithValue("@LocationTypeID", 1);
                        using (SqlDataReader resp = command.ExecuteReader())
                        {
                            var resultList = resp.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                customerId = Convert.ToInt32(x["locationid"]),
                                customerName = x["Location"].ToString()
                            });

                            return Request.CreateResponse(resultList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        

        [Route("api/selectcontract/CommitContracts")]
        [HttpPost]
        public string CommitContracts([FromBody] string [] val)
        {
            try
            {
                var cm = new CookieManager.SetIfoxCookie();
                var output = JsonConvert.SerializeObject(val);
                List<char> charsToRemove = new List<char>() { ' ', '"', '[', ']' };
                foreach (char c in charsToRemove)
                {
                    output = output.Replace(c.ToString(), String.Empty);
                }
                cm.OpsContractName(output);
                return "success";
            }
            catch (Exception ex)
            {
                return "error occurred:" + ex.Message;
            }
        }


        // POST api/<controller>
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}