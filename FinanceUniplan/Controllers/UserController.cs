﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unitrans.BusinessLayer.Contracts;
using Unitrans.Common.Helpers;
using UnitransTools.Classes;
using Newtonsoft.Json;

namespace FinanceUniplan.Controllers
{
    public class UserController : ApiController
    {
        private Unitrans.Common.Helpers.CookieManager.GetIfoxCookie GetIfoxCookie { get { return new Unitrans.Common.Helpers.CookieManager.GetIfoxCookie(); } }
        //api/user/getloggeduser
        public HttpResponseMessage GetLoggedUser()
        {
            try
            {
                var name = GetIfoxCookie.IFOXLoggedInUser;
                var email = GetIfoxCookie.IFOXEMail;
                var userinfor = new { Name = name, Email = email };
                //var pet = new { Data = ErrStatus = contractList, AdditionalParameters = ErrMessage = errormsg };
                return Request.CreateResponse(userinfor);
                
            }
            catch (Exception ex)
            {
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        // POST: api/User
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/User/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
    }
}
