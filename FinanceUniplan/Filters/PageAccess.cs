﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unitrans.BusinessLayer.Users;
using Unitrans.Common.Authentication;
using Unitrans.Common.Helpers;

namespace UnitransTools.Filters
{

   // public class PageAccess : ActionFilterAttribute
 public class PageAccessAttribute : ActionFilterAttribute
   
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private CookieManager.GetIfoxCookie GetIfoxCookies
        {
            get { return new CookieManager.GetIfoxCookie(); }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
           IfoxUserRights.UserRights ur = new IfoxUserRights.UserRights(GetIfoxCookies.OperatingContractID).RetrieveUserAccess(GetIfoxCookies.OperatingContractID);

           filterContext.Controller.ViewBag.PageAccess = ur.HasWriteAccess;
           filterContext.Controller.ViewBag.Modules = ur.Modules;

        }
    }

}