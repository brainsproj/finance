﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class BatchReportFilter
    {
        public int CustomerID { get; set; }
        public string FromDate { get; set; }
        public string BatchNumber { get; set; }
        public string ToDate { get; set; }
        public string BatchStatus { get; set; }
    }
}