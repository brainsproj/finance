﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class BatchRevenue
    {
        public string BatchCategory { get; set; }
        public string CategoryItem { get; set; }
        public string DivisionID { get; set; }
        public string RevenueIDs { get; set; }
        public string IndexIDs { get; set; }
    }
}