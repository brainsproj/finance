﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class Batching
    {
        public string BatchNumber { get; set; }
        public string GUID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ContractID { get; set; }
        public string CustomerID { get; set; }
        public string Variable { get; set; }
        public string Fixed { get; set; }
        public string ORC { get; set; }

        public string drpDestination { get; set; }
        public string drpDriver { get; set; }
        public string drpMeasurementUnit { get; set; }
        public string drpProduct { get; set; }
        public string drpTrailer { get; set; }
        public string drpTruck { get; set; }
        public string drpUpliftPoint { get; set; }
        public string drpSubContractor { get; set; }
        public string txtSearch { get; set; }

        public int DivisionID { get; set; }
        public int rateValueSelected { get; set; }
        public int variableRadioValue { get; set; }
        public int fixedRadioValue { get; set; }
        public int orcRadioValue { get; set; }

        public string RevenueIDs { get; set; }
        public string BatchData { get; set; }
        public int RateID { get; set; }
        public int DropID { get; set; }
        public int TripID { get; set; }
    }
}

