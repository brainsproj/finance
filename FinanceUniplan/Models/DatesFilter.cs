﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class DatesFilter
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}