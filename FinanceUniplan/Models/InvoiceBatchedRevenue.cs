﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class InvoiceBatchedRevenue
    {
        public string BatchNumber { get; set; }
        public string BatchAmount { get; set; }
        public string BatchedBy { get; set; }
        public string DateBatched { get; set; }
        public string InvoiceNo { get; set; }
        public string FinancePeriod { get; set; }
        public string Customer { get; set; }
        public int DivisionID { get; set; }
        public string Period { get; set; }
        public int SubRegionID { get; set; }
        public string Company { get; set; }
        public string GroupBatchNo { get; set; }
        public string CustInvRefNo { get; set; }
        public string InvoiceDate { get; set; }
        public string SAGEInvoiceTemplateID { get; set; }
    }
}