﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class InvoiceBatchedRevenueFilter
    {
        public int CustomerID { get; set; }
        public string OpsContractIDList { get; set; }
        public string SearchText { get; set; }
    }
}