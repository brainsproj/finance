﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class RatesFilter
    {
        public int ContractID { get; set; }
        public int CustomerID { get; set; }
        public int Status { get; set; }
    }
}