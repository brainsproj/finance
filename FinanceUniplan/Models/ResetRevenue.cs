﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class ResetRevenue
    {
        public string CurrentStatus { get; set; }
        public string NewStatus { get; set; }
        public string QueryType { get; set; }
        public string QueryValue { get; set; }
        public string BatchNumber { get; set; }
        public string RevenueID { get; set; }
    }
}