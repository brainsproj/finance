﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceUniplan.Models
{
    public class RevenueResetFilters
    {
        public int category { get; set; }
        public string searchtext { get; set; }
    }
}